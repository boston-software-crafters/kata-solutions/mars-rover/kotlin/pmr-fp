# Mars Rover Kata Starter Project for Kotlin using IntelliJ and Gradle

## Kata Instructions

Complete instructions can be found on the [Katalyst Site](https://katalyst.codurance.com/mars-rover) which includes a video of Sandro Mancuso solving the Mars Rover Kata.

You are encouraged to watch the video, even before attempting it yourself, but this is optional. If you do so, try to take a different approach when doing your solution.

In fact, if you have the time, check out any number of solutions ahead of time. The solution is not that important. The TDD process, once you have a solution in mind, is important, as is collaborating with your team.

So if you do look at other solutions, please avoid looking at the associated tests. That defeats the whole point of the exercise. Comparing after the fact might be a good thing to do.

The first step in solving the kata is to understand the constraints. Once you think you understand them, list your assumptions. Sandro does a nice job at this near the beginning of his video.

With your basic understanding, then start devising tests that yield the production code that solves the problem. The examples on the Katalyst Site are a good choice for tests.

As you build a suite of tests, you might see patterns emerge that are good to use in your testing. Go for it.

But most of all, have some fun.

## Challenges

If you are already in the Internet Hall Of Fame and have finished this kata before everyone else has read the **README.md** file, then here are some challenges for you:

1) Do a functional programming style solution if you've already done an OO solution. Or vice-versa. Which solution is better? faster? 

2) Add another rover device and a Pair of move instructions then write a program to determine if the rovers will collide.

3) Make your solution clean, as in the principles and practices of Robert "Uncle Bob" Martin and his Clean Code world: use good names, short functions, good structure, SOLID code, etc.

## Solution Notes

In this implementation we start with the following assumptions:

1) A single Mars Rover robot is placed on a 10x10 grid with a function named `execute()` calculating the position and
direction for the rover after a sequence of moves.

2) The 10x10 grid is addressed by x:y where 0,0 is the bottom, left position; x increments to the east, y increments to
the north and both wrap at the grid edge.

3) The implementation consists of one top level function with the signature:

    fun execute(input: String, position: Pair<Int, Int> = Pair(0, 0), direction: Char = 'N', obstacles: List<Pair<Int, Int>> = listOf()): String

where `input` is a string representing the possible moves:

'M' (move one step in the current direction),

'L' (turn to face left),

'R' (turn to face right)

with all other characters being ignored.

`position` is the initial x, y coordinates for the rover on the grid.

`obstacles` is a list of positions where movement is halted. If no obstacles are encountered, the output is a formatted string showing the current position and direction in the form x:y:direction, e.g. "3:7:E". When an
obstacle is encountered the output is of the form 0:x:y:direction, e.g. "0:1:3:W" where "x" and "y" are the position where the move action `M` started.
