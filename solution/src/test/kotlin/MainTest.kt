package com.pajato.katas

import kotlin.test.Test
import kotlin.test.assertEquals

class MainTest {

    @Test fun `when all basic valid and invalid moves are input verify the correct output`() {
        val parameters = mapOf(
            "" to "0:0:N",
            "A" to "0:0:N",
            "R" to "0:0:E",
            "RR" to "0:0:S",
            "RRR" to "0:0:W",
            "RRRR" to "0:0:N",
            "L" to "0:0:W",
            "LL" to "0:0:S",
            "LLL" to "0:0:E",
            "LLLL" to "0:0:N",
            "M" to "0:1:N",
            "MM" to "0:2:N",
            "MMMMMMMMMM" to "0:0:N",
            "MMRMMLM" to "2:3:N",
            "LMMM" to "7:0:W",
            "RRMMM" to "0:7:S",
            "MAM" to "0:2:N"
        )
        for (entry in parameters) assertEquals(entry.value, execute(entry.key))
    }

    @Test fun `when an obstacle is present verify the correct output`() {
        val parameters = mapOf(
            "MMM" to "O:0:2:N"
        )
        for (entry in parameters) assertEquals(entry.value, execute(entry.key, obstacles = listOf(Pair(0, 3))))
    }
}